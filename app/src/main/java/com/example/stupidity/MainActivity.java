package com.example.stupidity;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View;
import android.view.Window;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity {
    SeekBar simpleSeekBar;
    View    MyView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        simpleSeekBar = (SeekBar) findViewById(R.id.simpleSeekBar);
        MyView = (View) findViewById(R.id.myview);
        setcolor(510);
        simpleSeekBar.getProgressDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        simpleSeekBar.getThumb().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        simpleSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
                setcolor(progress);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }
    void setcolor(int var)	//this function computes the color scale a
    {

        int akwakwak= 255;	//we divide by 4  cause we have 4 transitions to scale RGB (0,0,1)->(0,1,1)->(0,1,0)->(1,1,0)->(1,0,0)
        if(var<akwakwak)
        {
            MyView.setBackgroundColor(Color.rgb(0, var,255));
        }
        else if(var<2*akwakwak)
        {
            var-=akwakwak;
            MyView.setBackgroundColor(Color.rgb(0,255,255-var));
        }
        else if(var<3*akwakwak)
        {
            var-=2*akwakwak;
            MyView.setBackgroundColor(Color.rgb(var,255,0));
        }
        else if(var<4*akwakwak)
        {
            var-=3*akwakwak;
            MyView.setBackgroundColor(Color.rgb(255,255- var,0));
        }
        else
        {
            MyView.setBackgroundColor(Color.rgb(255,0,0));
        }
    }

}


